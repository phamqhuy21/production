CREATE FUNCTION postgres_gin_pending_list_size() returns table(index_name text, pending_pages int8) AS $$
  WITH cte_gin_info AS (
    SELECT i.relname AS relname
    FROM pg_index ix
      LEFT JOIN pg_class i ON ix.indexrelid = i.oid
      LEFT JOIN pg_class t ON ix.indrelid = t.oid
      LEFT JOIN pg_am a ON i.relam = a.oid
    WHERE amname = 'gin'
  )
  SELECT relname::text AS index_name, n_pending_pages * current_setting('block_size')::bigint AS pending_pages
  FROM cte_gin_info, gin_metapage_info(get_raw_page(relname, 0));
$$ language sql security definer;