#! /opt/gitlab/bin/gitlab-ruby
# frozen_string_literal: true

# Runtime example:
#
#    sudo /var/opt/gitlab/scripts/find_dirty_git_repository_configs.rb --limit=200
#    2022-02-12 22:18:02 INFO  Taking repository inventory
#    2022-02-12 22:18:04 INFO  Elapsed time: 2.407443955 seconds
#    2022-02-12 22:18:04 INFO  Compiling config details
#    2022-02-12 22:18:05 INFO  Scanned 200/24789 (0%) git repository configs
#    2022-02-12 22:18:05 INFO  Elapsed time: 0.778788308 seconds
#    2022-02-12 22:18:05 INFO  Empty configs: 186/24789
#    2022-02-12 22:18:05 INFO  Errors: 0/24789
#    2022-02-12 22:18:05 INFO  Error log: /tmp/find_dirty_git_repository_configs.rb-errors-2022-02-12_221802.txt
#    2022-02-12 22:18:05 INFO  Untidy configs: /tmp/config_details.json
#

require 'fileutils'
require 'json'
require 'logger'
require 'open3'
require 'optparse'

# The Script module
module Script
  TIMESTAMP = Time.now.strftime('%Y-%m-%d_%H%M%S')
  module Default
    CONFIG = {
      allowed_config: 'remote.geo.url,remote.geo.fetch,remote.geo.mirror,remote.geo.prune',
      inventory_dir_path: File.join(__dir__, 'inventory.d'),
      inventory_file_path: File.join(__dir__, 'inventory.d', "shard-git-repositories-#{TIMESTAMP}.txt"),
      config_details_file_path: File.join('/', 'tmp', 'config_details.json'),
      errors_file_path: File.join('/', 'tmp', File.basename($PROGRAM_NAME) + "-errors-#{TIMESTAMP}.json"),
      unique_config_entries_file_path: File.join('/', 'tmp', 'unique_config_entries.json'),
      inventory_command: 'ionice --class 2 --classdata 7 find ' \
        '/var/opt/gitlab/git-data/repositories/@hashed/ -maxdepth 3 ' \
        '-name *.git -not -name *.wiki.git -not -name *+deleted.git -not -name *.design.git ' \
        '-type d > %<inventory_file_path>s',
      read_config_command: 'git config --name-only --get-regex --local ' \
        '\'^(http\..+\.extraheader|remote\..+\.(fetch|mirror|prune|url)|core\.' \
        '(commitgraph|sparsecheckout|splitindex))$\'',
      status_template: 'Scanned %<iteration>s/%<total>s (%<percentage>s%%) git repository configs',
      use_latest_cached_inventory: false,
      limit: Float::INFINITY,
      log_level: Logger::INFO
    }.freeze
  end
end

# The Logging module
module Logging
  LOG_TIMESTAMP_FORMAT = '%Y-%m-%d %H:%M:%S'
  LOG_FORMAT = "%<timestamp>s %-5<level>s %<msg>s\n"
  DEBUG_LOG_FORMAT = "\n#{LOG_FORMAT}"
  PROGRESS_LOG_FORMAT = "\r%<timestamp>s %-5<level>s %<msg>s"

  def formatter_procedure(format_template = LOG_FORMAT)
    proc do |level, t, _name, msg|
      format(format_template, timestamp: t.strftime(LOG_TIMESTAMP_FORMAT), level: level, msg: msg)
    end
  end

  def initialize_log(formatter = formatter_procedure)
    $stdout.sync = true
    log = Logger.new($stdout)
    log.level = Logger::INFO
    log.formatter = formatter
    log
  end

  def log
    @log ||= initialize_log
  end

  def debug_log
    @debug_log ||= initialize_log(formatter_procedure(DEBUG_LOG_FORMAT))
  end

  def progress_log
    @progress_log ||= initialize_log(formatter_procedure(PROGRESS_LOG_FORMAT))
  end

  def debug_command(cmd)
    debug_log.debug "Command: #{cmd}"
    cmd
  end
end

# The GitRepositoryInventoryInitializationMethods module
module GitRepositoryInventoryInitializationMethods
  FIELDS = %i[
    limit inventory_command read_config_command allowed_config
    status_template config_details_file_path inventory_file_path
    inventory_dir_path errors_file_path unique_config_entries_file_path
    use_latest_cached_inventory
  ].freeze
  FIELDS.each { |key| attr_reader(key) }
  MEMBERS = %i[
    errors empty_configs unique_config_entries inventory_file_path
    total_repositories
  ].freeze
  MEMBERS.each { |key| attr_accessor(key) }

  def init(options)
    @empty_configs = 0
    @unique_config_entries = Hash.new(0)
    @errors = []
    log.level = debug_log.level = options[:log_level]
    configure(options)
  end

  def configure(options)
    FIELDS.each { |key| instance_variable_set("@#{key}", options[key]) }
  end
end
# module GitRepositoryInventoryInitializationMethods

# The GitRepositoryInventoryInstanceMethods module
module GitRepositoryInventoryInstanceMethods
  def run_command(cmd)
    debug_command(cmd)
    Open3.capture3(cmd)
  end

  def take_inventory
    log.info 'Taking repository inventory'
    start = Time.now
    compile_repository_inventory unless use_latest_cached_inventory
    self.inventory_file_path = select_inventory_file
    self.total_repositories = count_lines(inventory_file_path)
    log.info "Elapsed time: #{Time.now - start} seconds"
  end

  def build_inventory_command
    format(inventory_command, inventory_file_path: inventory_file_path)
  end

  def build_read_config_error_message(stdout, stderr, status, path)
    if !stderr&.empty? then stderr
    elsif !stdout&.empty? then stdout
    else "Unknown error code #{status.exitstatus} reading repository config: #{path}"
    end
  end

  def read_config(dir_path)
    stdout, stderr, status = run_command(read_config_command)
    return '' if status.exitstatus == 1 && !stdout.nil? && stdout.empty?

    raise build_read_config_error_message(stdout, stderr, status, dir_path) unless status.success?

    index_config_entries(stdout&.strip)&.join(',') || ''
  end

  def index_config_entries(config)
    entries = config&.split("\n")
    entries.each { |key| unique_config_entries[key] += 1 }
    entries
  end

  def compile_repository_inventory
    run_command(build_inventory_command)
  end

  def ensure_inventory_dir_exists
    FileUtils.mkdir_p(inventory_dir_path) unless Dir.exist?(inventory_dir_path)
  end

  def select_inventory_file(file_path = inventory_file_path)
    ensure_inventory_dir_exists

    unless File.exist?(file_path)
      Dir.glob(File.join(inventory_dir_path, '*')).max_by(1) do |f|
        file_path = f
      end
    end

    file_path
  end

  def count_lines(file_path)
    File.exist?(file_path) ? File.read(file_path).each_line.count : 0
  end

  def get_repository_config(dir_path)
    Dir.chdir(dir_path) { read_config(dir_path) }
  end

  def generate_details(dir_path, data)
    { repository_path: dir_path, config: data }
  end

  def analyze_repository_config(dir_path)
    result = get_repository_config(dir_path)

    if result.nil? || result.empty? then self.empty_configs += 1
    elsif allowed_config.match?(result) then return
    end

    generate_details(dir_path, result)
  rescue StandardError => e
    errors << { repository_path: dir_path, error: e.message }
    nil
  end

  def read_inventory(file_path, &block)
    i = 0
    File.readlines(file_path, chomp: true).each do |line|
      break if (i += 1) > limit

      block.call(line, i)
    end
  end

  def clear_config_details
    FileUtils.rm(config_details_file_path) if File.exist?(config_details_file_path)
  end

  def new_line
    print "\n"
  end

  def process_inventory
    clear_config_details
    File.open(config_details_file_path, 'a') do |output_file|
      read_inventory(inventory_file_path) do |repository_dir_path, line_number|
        save(output_file, yield(repository_dir_path, line_number))
      end
    end
    new_line
  end

  def persist_data(file, data)
    file.puts(JSON.dump(data))
  end

  def save(file, data)
    return if data.nil? || data&.values&.any? { |val| val.nil? || val.empty? }

    persist_data(file, data)
  end

  def persist_errors
    File.open(errors_file_path, 'w') { |f| errors.each { |e| f.puts(e.to_json) } } unless errors.empty?
  end

  def persist_unique_config_entries
    File.open(unique_config_entries_file_path, 'a') do |output_file|
      persist_data(output_file, unique_config_entries)
    end
  end

  def status(itr, total = total_repositories)
    { iteration: itr, total: total, percentage: total.positive? ? ((itr / total.to_f) * 100).to_i : 0 }
  end

  def display_status(iteration)
    progress_log.info format(status_template, **status(iteration))
  end

  def with_report(start = Time.now, report = [])
    yield
    persist_unique_config_entries
    report << "Elapsed time: #{Time.now - start} seconds"
    report << "Empty configs: #{empty_configs}/#{total_repositories}"
    report << "Errors: #{errors.length}/#{total_repositories}"
    report << "Error log: #{errors_file_path}" unless errors.empty?
    report << "Unique config entries: #{unique_config_entries_file_path}"
    report << "Untidy configs: #{config_details_file_path}"
    report.each { |msg| log.info msg }
  end

  def find_empty_configs
    take_inventory
    log.info 'Compiling config details'
    with_report do
      process_inventory do |repository_dir_path, line_number|
        display_status(line_number)
        analyze_repository_config(repository_dir_path)
      end
      persist_errors
    end
    nil
  end
end
# module GitRepositoryInventoryInstanceMethods

# The GitRepositoryInventory class
class GitRepositoryInventory
  include Logging
  include GitRepositoryInventoryInitializationMethods
  include GitRepositoryInventoryInstanceMethods

  def initialize(options = {})
    init(options)
  end
end

# The Script module
module Script
  def parse_arguments(options = Script::Default::CONFIG.dup)
    OptionParser.new do |parser|
      parser.banner = "Usage: #{File.basename($PROGRAM_NAME)} [options]"
      parser.on('--latest', 'Use latest cached inventory') do
        options[:use_latest_cached_inventory] = true
      end
      parser.on('-l', '--limit=<n>', Integer, 'Limit repositories scanned') do |v|
        options[:limit] = v
      end
      parser.on('--allowed-config=<text>', 'Allowed git config (comma-delimited); ' \
        'default: ' + options[:allowed_config]) do |v|
        options[:allowed_config] = v
      end
      parser.on('--inventory-directory=<path>', 'Directory in which to store inventory caches; ' \
        'default: ' + options[:inventory_dir_path]) do |v|
        options[:inventory_dir_path] = v
      end
      parser.on('-i', '--inventory-file=<path>', 'Use this inventory cache file; ' \
        'default: ' + options[:inventory_file_path]) do |v|
        options[:inventory_file_path] = v
      end
      parser.on('-o', '--output=<path>', 'Where to store git config report; ' \
        'default: ' + options[:config_details_file_path]) do |v|
        options[:config_details_file_path] = v
      end
      parser.on('-v', '--verbose', 'Increase verbosity') do
        options[:log_level] -= 1
      end
      parser.on_tail('-?', '--help', 'Show this message') do
        puts parser
        exit
      end
    end.parse!
    options
  end

  def main(args = parse_arguments)
    puts "User may interrupt with Ctrl+C"
    git_repository_inventory = GitRepositoryInventory.new(args)
    git_repository_inventory.find_empty_configs
  rescue Interrupt => e
    $stdout.write "\r\n#{e.class}\n"
    $stdout.flush
    exit 0
  end
end
# module Script

Object.new.extend(Script).main if $PROGRAM_NAME == __FILE__
